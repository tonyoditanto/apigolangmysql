CREATE DATABASE db_apigo;
USE db_apigo;
CREATE USER `tony`@`localhost` IDENTIFIED BY '12345678';
GRANT ALL PRIVILEGES ON db_apigo.* TO `tony`@`localhost`;
FLUSH PRIVILEGES; 
-- mysql -u user04 -p be04

CREATE TABLE mahasiswa (
	`id` INT(8) AUTO_INCREMENT NOT NULL PRIMARY KEY,
	`nim` INT(11) NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`semester` VARCHAR(255) NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL);